# Simple opus encoder

A simple Opus https://opus-codec.org/  encoder


# Requirements

Debian and variants:

```sh
	 sudo apt-get install libasound2-dev libvorbis-dev libopus-dev build-essential opus-tools luarocks luajit
```

Luarocks needed

```sh
sudo luarocks install lzmq luallthreads2
```

Also libopusenc

```sh
git clone https://github.com/xiph/libopusenc
cd libopusenc
./autogen.sh
./configure
make
sudo make install
sudo ldconfig
```


# Build

```sh
	 make
```

# Test (ish)

```sh
	make test-repeatable
```

# Pictures and videos

- [Video](https://hub.spaz.org/media/9e5b9266891dddd51728161f5e0d9b92d6c0a2094a44404be192478c75af3ec1.mp4?name=VID_20190914_180455.mp4)

- [Ugly hand-made prototype](https://hub.spaz.org/media/e0f8196ff26d39ea93350d6b0b7bd34ded490a1a2c647256d343860dcdae7779.jpg?name=randomTemp12610855988091876500.jpg)
