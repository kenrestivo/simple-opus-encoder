;;; single file (headers included) to make compliation easier, but should be moved out

(global ffi (require :ffi))

;; via https://stackoverflow.com/questions/20943322/accessing-keys-from-linux-input-device

;; input events
(ffi.cdef "
typedef int32_t __time_t;
typedef uint32_t __useconds_t;
typedef int32_t __suseconds_t;

struct timeval
{
  __time_t tv_sec;
  __suseconds_t tv_usec;
};


struct input_event {
 struct timeval time;
 uint16_t type;
 uint16_t code;
 int32_t value;
};
")

;; file io
(ffi.cdef "
typedef uint32_t size_t;
typedef int32_t ssize_t;

ssize_t read(int fd, void *buf, size_t count);

int open(const char *pathname, int flags);
int close (int fd);
")


;; poll
(ffi.cdef "
typedef enum _poll_consts {
 POLLIN   =   0x0001,
 POLLPRI    =   0x0002,
 POLLOUT = 0x0004,
 POLLERR = 0x0008,
 POLLHUP = 0x0010,
 POLLNVAL = 0x0020,
 POLLRDNORM = 0x0040,
 POLLRDBAND = 0x0080,
 POLLWRNORM = 0x0100,
 POLLWRBAND = 0x0200,
 POLLMSG = 0x0400,
 POLLREMOVE = 0x1000,
 POLLRDHUP     =    0x2000
} poll_consts_t;


typedef uint32_t nfds_t;


struct pollfd {
               uint32_t   fd;         /* file descriptor */
               int16_t events;     /* requested events */
               int16_t revents;    /* returned events */
           };

int poll(struct pollfd *fds, nfds_t nfds, int timeout);
")

(ffi.cdef "
    int ioctl(int fd, unsigned long request, ...);
    int fileno(void *stream);

    typedef enum _i2c_consts {
       I2C_RETRIES  =  0x0701,
       I2C_TIMEOUT,
       I2C_SLAVE,
       I2C_TENBIT,
       I2C_FUNCS,
       I2C_SLAVE_FORCE,
       I2C_RDWR,
       I2C_PEC,
       I2C_SMBUS  = 0x0720,
       I2C_RDWR_IOCTL_MAX_MSGS  = 42
    } i2c_consts_t;
   ")
