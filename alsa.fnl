(local ffi (require :ffi))
(local libalsa (require :lib-alsa))
(local utils (require :utils))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))
(require-macros :utils-macros)




(fn read
  [params]
  (local fennel (require :fennel))
  (table.insert (or package.loaders package.searchers) fennel.searcher)
  (var cnt 0) ;;; XXX NASTY
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        io (require :io)
        ffi (require :ffi)
        libalsa (require :lib-alsa)
        utils (require :utils)
        ctx (zthreads.get_parent_ctx)
        skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.pcm-addr}))
        ctl (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.alsa-control-addr}))]

    (utils.log "alsa connected => " params.pcm-addr)

    (fn calc-bytes-per-channel
      [params]
      (/ (syscall-pos (libalsa.snd_pcm_format_width params.format)) 8))

    (fn  calc-bufsiz
      [params bytes-per-channel]
      (* params.frames-to-buffer
         bytes-per-channel
         params.channels))


    (local capture-handle-pp (ffi.new "void *[1]" nil))
    (syscall-pos (libalsa.snd_pcm_open capture-handle-pp params.device-name ffi.C.SND_PCM_STREAM_CAPTURE 0))
    (local capture-handle-p (. capture-handle-pp 0))
    (local hw-params-pp (ffi.new "void *[1]" nil))
    
    (utils.log "mallocing")
    (syscall-pos (libalsa.snd_pcm_hw_params_malloc hw-params-pp))
    (local hw-params-p (. hw-params-pp 0))
    
    (utils.log "anying")
    (syscall-pos (libalsa.snd_pcm_hw_params_any capture-handle-p hw-params-p))
    
    (utils.log "setting access: " params.access)
    (syscall-pos (libalsa.snd_pcm_hw_params_set_access capture-handle-p hw-params-p params.access))

    (utils.log "setting format: " params.format)
    (syscall-pos (libalsa.snd_pcm_hw_params_set_format capture-handle-p hw-params-p params.format))

    ;; UGH. we have to use NEAR here, which means the rate we end up with could be different. So get it after.
    (utils.log "setting samplerate: " params.sample-rate)
    (syscall-pos (libalsa.snd_pcm_hw_params_set_rate_near capture-handle-p
                                                          hw-params-p
                                                          (ffi.new "unsigned int[1]" params.sample-rate)
                                                          (ffi.new "int [1]" 0)))

    
    (utils.log "setting channels: " params.channels)
    (syscall-pos (libalsa.snd_pcm_hw_params_set_channels capture-handle-p hw-params-p params.channels))

    ;; TODO: set period size!
    
    (utils.log "writing params")
    (syscall-non-nil (libalsa.snd_pcm_hw_params capture-handle-p hw-params-p))
    
    ;; after writing, NOW we have to find out what actually happened
    (let [time-p (ffi.new "unsigned int[1]")
          size-p (ffi.new "unsigned long[1]")
          rate-p (ffi.new "unsigned int[1]")]

      (syscall-pos (libalsa.snd_pcm_hw_params_get_rate hw-params-p
                                                      rate-p
                                                      (ffi.new "int [1]" 0)))
      (tset params :sample-rate (. rate-p 0)) ;; NOTE!! mutation! nasty!
      (utils.log "actual samplerate: " params.sample-rate)


      (syscall-pos (libalsa.snd_pcm_hw_params_get_period_size hw-params-p
                                                              size-p
                                                              (ffi.new "int [1]" 0)))
      (utils.log "actual period size (frames): " (tonumber (. size-p 0)))
      
      (syscall-pos (libalsa.snd_pcm_hw_params_get_period_time hw-params-p
                                                              time-p
                                                              (ffi.new "int [1]" 0)))
      (utils.log "actual period time (us): " (. time-p 0)))

    
    (utils.log "preparing handle")
    (syscall-non-nil (libalsa.snd_pcm_prepare capture-handle-p))


    
    (local bytes-per-channel (calc-bytes-per-channel params))
    (local bufsiz (calc-bufsiz params bytes-per-channel))
    (local buffer (ffi.new "uint8_t[?]" bufsiz))
    
    (utils.log "alsa bytes per channel: " bytes-per-channel)
    (utils.log "alsa bufsiz: " bufsiz)
    
    
    (fn recv
      [out-skt]
      (let [frames-read (syscall-pos (tonumber (libalsa.snd_pcm_readi capture-handle-p buffer params.frames-to-buffer)))
            bytes-read (* frames-read bytes-per-channel params.channels)]
        (when (not (= frames-read  params.frames-to-buffer))
          (utils.log "alsa UNDERRUN " (tonumber frames-read)))
        (utils.log "alsa <- " (tonumber bytes-read))
        ;; this may need error czeching too
        ;; XXX 8MB is too fucking big to send as a single string yo
        (out-skt:send (ffi.string buffer bytes-read)))
      
      (if (-?> (ctl:recv zmq.NOBLOCK) (= "DONE")) 
          (do
              (utils.log "alsa got DONE signal, ending")
            (out-skt:send_more "")
            (out-skt:send "DONE"))
          (recv out-skt)))
    


    ;; TODO: bufsiz and bytes-per-channel wil have to be transmitted up via zmq,
    ;; as it is setup from here, inside a thread!

    
    (utils.log "alsa sending ready")
    (ctl:send :ready) 
    (utils.log "alsa setup,waiting for start")
    (utils.wait-for-start ctl)

    (utils.log "alsa starting receive <= " params.device-name)

    (recv skt)
    
    (utils.log "alsa done")

    (utils.log "freeing params")
    (libalsa.snd_pcm_hw_params_free hw-params-p)

    (syscall-pos (libalsa.snd_pcm_close capture-handle-p))

    ;; TODO: linger?

    (utils.log "alsa thread ending, sockets closing")
    
    (skt:close)
    (ctl:close)))



(fn setup
  [params]
  (tset params :alsa-ctl-skt (zmq.assert (params.context:socket {1 zmq.PAIR
                                                                 :bind params.alsa-control-addr})))
  (utils.log "creating alsa thread")
  (tset params :alsa-thread (zthreads.run
                             params.context
                             read
                             {:access params.access
                              :alsa-control-addr params.alsa-control-addr
                              :channels params.channels
                              :device-name params.device-name
                              :format params.format
                              :frames-to-buffer params.frames-to-buffer
                              :pcm-addr params.pcm-addr
                              :sample-rate params.sample-rate}))
  params)


(fn teardown
  [params]
  (utils.log"alsa closing")
  (params.alsa-thread:join)
  (tset params :alsa-thread nil)
  (params.alsa-ctl-skt:close)
  (tset params :alsa-ctl-skt nil)
  params)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{: setup
 :lib libalsa ;; XXX NOTE NAME CHANGE
 : teardown}
