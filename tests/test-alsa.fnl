(local fennel (require :fennel))
(local lume (require :lume))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local alsa (require :alsa))
(local file (require :file))
(local opus (require :opus))
(local utils (require :utils))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local state {:params {
                       :access alsa.lib.SND_PCM_ACCESS_RW_INTERLEAVED
                       :alsa-control-addr "inproc://alsa-control"
                       :application opus.libs.libopus.OPUS_APPLICATION_AUDIO
                       :artist "Testing Opus Encoding"
                       :bit-depth 16 ;; this is implicit in SND_PCM_FORMAT_S16_LE
                       :bit-rate 96000
                       :bufsiz 4096 ;;; XXX hack, will need to come from alsa
                       :bytes-per-channel 2 ;; XXX HAAACK must come from alsa
                       :channels 2
                       :complexity 10
                       :device-name "hw:1,0"
                       :file-in-path "/tmp/input.pcm"
                       :file-out-path "/tmp/output.opus"
                       :format alsa.lib.SND_PCM_FORMAT_S16_LE
                       :frames-to-buffer 1024
                       :mapping 0 ;;; used for opus
                       :opus-addr "inproc://opus"
                       :pcm-addr "inproc://pcm"
                       :opus-control-addr "inproc://opus-control"
                       :file-in-control-addr "inproc://file-in-control"
                       :file-out-control-addr "inproc://file-out-control"
                       :sample-rate 48000
                       :title "Test Sample"
                       }})


(fn start
  [state]
  (tset state :params (alsa.setup state.params))
  (tset state :params (file.setup-out state.params))
  (tset state :params (opus.setup state.params))


  ;; horrilble, but works
  (: state.params.file-out-thread :start)
  (: state.params.file-out-ctl-skt :recv)


  (: state.params.opus-thread :start)
  (: state.params.opus-ctl-skt :recv)


  (: state.params.alsa-thread :start)
  (: state.params.alsa-ctl-skt :recv)


  ;; now start them all
  (: state.params.file-out-ctl-skt :send :start)
  (: state.params.opus-ctl-skt :send :start)
  (: state.params.alsa-ctl-skt :send :start)
  state)


(fn stop
  [state]
  (: state.params.alsa-ctl-skt :send :DONE)
  (tset state :params (file.teardown-out state.params))
  (tset state :params (opus.teardown state.params))
  (tset state :params (alsa.teardown state.params))
  state)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state.params :context  (zmq.context))

(start state)
(os.execute "sleep 2")
(stop state)

(: state.params.context :term)







