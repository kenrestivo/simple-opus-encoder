(global fennel (require :fennel))
(global lume (require :lume))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(global alsa (require :alsa))
(global file (require :file))
(global opus (require :opus))
(global zmq (require :lzmq))
(global zthreads (require :lzmq.threads))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global state {:params {
                        :application opus.libs.libopus.OPUS_APPLICATION_AUDIO
                        :artist "Testing Opus Encoding"
                        :bit-depth 16
                        :bit-rate 96000
                        :bufsiz 4096
                        :channels 2
                        :device-name "hw:1,0"
                        :file-in-path "/tmp/input.pcm"
                        :file-out-path "/tmp/output.opus"
                        :format alsa.lib.SND_PCM_FORMAT_S16_LE
                        :frames-to-buffer 1024
                        :mapping 0 ;;; used for opus
                        :opus-addr "inproc://opus"
                        :pcm-addr "inproc://pcm"
                        :sample-rate 48000
                        :title "Test Sample"
                        }
               })

(tset state.params :context  (zmq.context))

;; Force these to be the same
(tset state :params :opus-addr "inproc://test.inproc")
(tset state :params :pcm-addr "inproc://test.inproc")


(tset state :params (file.setup-out state.params))
(tset state :params (file.setup-in state.params))


(: state.params.file-out-thread :start)
(: state.params.file-in-thread :start)


(tset state :params (file.teardown-out state.params))
(tset state :params (file.teardown-in state.params))
