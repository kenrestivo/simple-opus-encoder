;; TODO: must require utils to use this, it has the ffi defs

(local ffi (require :ffi))




(fn syscall-pos
  [syscall ...]
  `(let  [rv# (do ,syscall ,...)]
     (assert (<= 0 rv#)  (-> rv#
                             math.abs
                             ffi.C.strerror
                             ffi.string))
     rv#))

(fn syscall-non-nil
  [syscall ...]
  `(let  [rv# (do ,syscall ,...)]
     (assert (not (= nil rv#))  "Got nil")
     rv#))

(fn lua-dual-return
  [syscall ...]
  `(let  [(rv# msg#) (do ,syscall ,...)]
     (assert (not (= nil rv#))  msg#)
     rv#))



{: lua-dual-return
 : syscall-pos
 : syscall-non-nil}
