;; not if compiling to single lua file
(global ffi (require :ffi))
(require :events-headers)

(require-macros :utils-macros)
(local utils (require :utils))

;; if not using poll, can use [2] and always throw the second one away, it's garbage
(global state {:input-ev (ffi.new "struct input_event[1]")
               :button-fd  (syscall-pos (ffi.C.open "/dev/input/by-path/platform-rec_buttons-event" 0))
               :pfds (ffi.new "struct pollfd[2]")
               :active-fds 0
               :nfds 1 ;; XXX nasty hack, signals whetehr liq is supposed to be running
               :led-path "/sys/class/leds/orangepi:green:stream/trigger"
               })
;; consts
;; NOTE 4 events for every button push type 0 and 1, and something else
(local valid-event-type 1) ;; type will always be 1 on a valid event
(local keydown 1) ;; value wil be 1 on keydown, 0 on keyup
(local keyup 0)

(fn event-as-map
  [state]
  {:code (. state.input-ev 0 :code)
   :value (. state.input-ev 0 :value)
   :type (. state.input-ev 0 :type)})

;; TODO: should really be in here not in a bash script
(fn set-led!
  [val]
  "Set the state of the LED in hardware."
  (os.execute (.. "/usr/local/bin/stream-led " val)))

(fn get-led
  [path]
  "Get actual state of LED from the hardware itself"
  (-> path
      utils.slurp
      (string.match "%[(.+)%]")))

(fn liq!
  [command]
  "Send a command to systemd to control liquidsoap"
  (os.execute (.. "sudo systemctl " command " liq")))


(fn flush-events
  [state]
  "flush any events sitting there unread, i.e. poll with tiny timeout"
  (utils.log "Checking for bottled-up events")
  (tset state :active-fds (syscall-pos (ffi.C.poll state.pfds state.nfds 1)))
  (when (< 0 state.active-fds)
    (utils.log "Flushing event")
    (syscall-pos (ffi.C.read state.button-fd state.input-ev
                             (ffi.sizeof state.input-ev)))
    (flush-events state)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; setup button
(tset (. state.pfds 0) :fd state.button-fd)
(tset (. state.pfds 0) :events ffi.C.POLLIN)

(flush-events state)

(while true
  ;; THIS does the poll
  (tset state :active-fds (syscall-pos (ffi.C.poll state.pfds state.nfds 1000)))

  (when (< 0 state.active-fds)
    
    ;; button
    (when (< 0 (bit.band (. state :pfds 0 :revents) ffi.C.POLLIN))
      (syscall-pos (ffi.C.read state.button-fd state.input-ev
                               (ffi.sizeof state.input-ev)))
      ;; (utils.log (. state.ev 0 :value))
      (when (and (= valid-event-type (. state :input-ev 0 :type))
                 ;; this is the button up event?
                 (= keydown (. state :input-ev 0 :value))) ;; XXX ugly

        ;; set the state by *reading* the [] of the led state file!
        (if (= :none (get-led state.led-path))
            (do
              (set-led! :heartbeat) ;; because we are starting, instant response to user. liq will also set to heatbeat anyway
              (liq! :start)) 
            (do
              (liq! :stop)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ffi.C.close state.button-fd)


