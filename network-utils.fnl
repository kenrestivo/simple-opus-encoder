(local socket (require :socket))
(local zmq (require :lzmq))
(local lume (require :lume))
(local zthreads (require :lzmq.threads))
(local utils (require :utils))
(require-macros :utils-macros)


(fn connect
  [params]
  (utils.log "network connecting " params.server-addr ":" params.server-port)
  (let [(conn err) (socket.connect params.server-addr params.server-port)]
    ;; TODO: errors!!
    (when err (print err))
    conn))

(fn send-ready?
  [conn timeout-secs]
  (let [(recv snd) (socket.select [] [conn] timeout-secs)]
    (< 0 (# snd))))

(fn more?
  [conn timeout-secs]
  (let [(recv snd) (socket.select [conn] [] timeout-secs)]
    (< 0 (# recv))))

(fn drain-conn
  [conn timeout-secs acc]
  (if (more? conn timeout-secs)
      (let [(res err) (conn:receive)]
        ;; TODO err handling, i.e. (nil :closed)
        (when err (utils.log err))
        (utils.log (string.format "network received: [%s]" res))
        (if (= res "") ;; EOF is empty string the way socket gets HTTP
            acc
            (drain-conn conn timeout-secs (lume.concat (or acc []) [res]))))
      acc))

;; TODO: basic auth base64 plz!


(fn make-login
  [params]
  (string.format "SOURCE %s HTTP/1.0\r
ice-url: \r
ice-name: %s - %s\r
Authorization: Basic %s\r
ice-description: %s\r
ice-public: 1\r
User-Agent: simple-opus/0.0.1 (Unix; Fennel 0.3.0)\r
ice-genre: %s\r
ice-audio-info: \r
Content-Type: application/ogg\r
\r
"
                 params.mountpoint
                 params.artist
                 params.title
                 params.basic-auth
                 (or params.description "")
                 (or params.genre "")))


(fn login
  [params]
  (let [conn (connect params)]
    ;; TODO: basic auth too
    (conn:send (make-login params))
    (let [resp (drain-conn conn params.timeout-secs)]
      (utils.log (string.format "[%s]" resp))
      conn)))








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{
 : login
 : send-ready?
 }


