(local fennel (require :fennel))
(local lume (require :lume))
(table.insert (or package.loaders package.searchers) fennel.searcher) ;; only needed at top levels
(local file (require :file))
(local alsa (require :alsa))
(local socket (require :socket))
(local opus (require :opus))
(local network (require :network))
(local network-utils (require :network-utils))
(local utils (require :utils))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local state {:params {
                       :access alsa.lib.SND_PCM_ACCESS_RW_INTERLEAVED
                       :alsa-control-addr "inproc://alsa-control"
                       :application opus.libs.libopus.OPUS_APPLICATION_AUDIO
                       :artist "Testing Opus Encoding"
                       :basic-auth "c291cmNlOmRvbnRoYWNrbWU="  ;; HACK HACK NO, calculate it on the fly from username/pwd
                       :bit-depth 16 ;; this is implicit in SND_PCM_FORMAT_S16_LE
                       :bit-rate 48000
                       :bufsiz 4096 ;;; XXX hack, will need to come from alsa
                       :bytes-per-channel 2 ;; XXX HAAACK must come from alsa
                       :channels 2
                       :complexity 10
                       :device-name "hw:1,0"
                       :file-in-path "/tmp/hi-res.pcm"
                       :file-out-path "/tmp/output.opus"
                       :format alsa.lib.SND_PCM_FORMAT_S16_LE
                       :frames-to-buffer 1024
                       :mapping 0 ;;; used for opus
                       :mountpoint "/stream"
                       :network-control-addr "inproc://network-control"
                       :opus-addr "inproc://opus"
                       :pcm-addr "inproc://pcm"
                       :opus-control-addr "inproc://opus-control"
                       :file-in-control-addr "inproc://file-in-control"
                       :file-out-control-addr "inproc://file-out-control"
                       :sample-rate 48000
                       :server-addr "localhost"
                       :server-port 8010
                       :title "Test Sample"
                       :timeout-secs 10
                       }})

(fn start
  [state]
  (tset state :params (file.setup-in state.params))
  (tset state :params (network.setup state.params))
  (tset state :params (opus.setup state.params))


  ;; horrilble, but works
  (: state.params.network-thread :start)
  (: state.params.network-ctl-skt :recv)


  (: state.params.opus-thread :start)
  (: state.params.opus-ctl-skt :recv)

  (: state.params.file-in-thread :start)
  (: state.params.file-in-ctl-skt :recv)


  (: state.params.network-ctl-skt :send :start)
  (: state.params.opus-ctl-skt :send :start)
  (: state.params.file-in-ctl-skt :send :start)
  state)


(fn stop
  [state]  
  (tset state :params (network.teardown state.params))
  (tset state :params (opus.teardown state.params))
  (tset state :params (file.teardown-in state.params))
  state)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state.params :context  (zmq.context))

(start state)
;; TODO: sleep? accept a stop message?
;; TODO: will also need an endless read for network
(stop state)

(: state.params.context :term)







