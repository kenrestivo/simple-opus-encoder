(local ffi (require :ffi))
(local libopus (ffi.load :libopus))
(local libopusenc (ffi.load :libopusenc))

(ffi.cdef "

typedef enum _opus_application {
    OPUS_APPLICATION_VOIP = 2048,
    OPUS_APPLICATION_AUDIO = 2049,
    OPUS_APPLICATION_RESTRICTED_LOWDELAY = 2051
} opus_application_t;

typedef enum _opus_encoder_ctls {
OPUS_SET_APPLICATION_REQUEST  = 4000,
OPUS_GET_APPLICATION_REQUEST,
OPUS_SET_BITRATE_REQUEST,
OPUS_GET_BITRATE_REQUEST,
OPUS_SET_MAX_BANDWIDTH_REQUEST,
OPUS_GET_MAX_BANDWIDTH_REQUEST,
OPUS_SET_VBR_REQUEST,
OPUS_GET_VBR_REQUEST,
OPUS_SET_BANDWIDTH_REQUEST,
OPUS_GET_BANDWIDTH_REQUEST,
OPUS_SET_COMPLEXITY_REQUEST,
OPUS_GET_COMPLEXITY_REQUEST,
OPUS_SET_INBAND_FEC_REQUEST,
OPUS_GET_INBAND_FEC_REQUEST,
OPUS_SET_PACKET_LOSS_PERC_REQUEST,
OPUS_GET_PACKET_LOSS_PERC_REQUEST,
OPUS_SET_DTX_REQUEST,
OPUS_GET_DTX_REQUEST,
OPUS_SET_VBR_CONSTRAINT_REQUEST    =  4020,
OPUS_GET_VBR_CONSTRAINT_REQUEST,
OPUS_SET_FORCE_CHANNELS_REQUEST,
OPUS_GET_FORCE_CHANNELS_REQUEST,
OPUS_SET_SIGNAL_REQUEST,
OPUS_GET_SIGNAL_REQUEST,
OPUS_GET_LOOKAHEAD_REQUEST       =    4027,
OPUS_GET_SAMPLE_RATE_REQUEST     =    4029,
OPUS_GET_FINAL_RANGE_REQUEST     =    4031,
OPUS_GET_PITCH_REQUEST           =    4033,
OPUS_SET_GAIN_REQUEST            =    4034,
OPUS_SET_LSB_DEPTH_REQUEST       =    4036,
OPUS_GET_LSB_DEPTH_REQUEST       =    4037,
OPUS_GET_LAST_PACKET_DURATION_REQUEST = 4039,
OPUS_SET_EXPERT_FRAME_DURATION_REQUEST,
OPUS_GET_EXPERT_FRAME_DURATION_REQUEST,
OPUS_SET_PREDICTION_DISABLED_REQUEST,
OPUS_GET_PREDICTION_DISABLED_REQUEST,
OPUS_GET_GAIN_REQUEST              =  4045,
OPUS_SET_PHASE_INVERSION_DISABLED_REQUEST,
OPUS_GET_PHASE_INVERSION_DISABLED_REQUEST

} opus_encoder_ctls_t;


typedef enum _opus_more_stuff {

 OPUS_BANDWIDTH_NARROWBAND         =   1101,
 OPUS_BANDWIDTH_MEDIUMBAND,
 OPUS_BANDWIDTH_WIDEBAND,
 OPUS_BANDWIDTH_SUPERWIDEBAND,
 OPUS_BANDWIDTH_FULLBAND,

 OPUS_SIGNAL_VOICE                 =   3001,
 OPUS_SIGNAL_MUSIC,


 OPUS_FRAMESIZE_ARG                =   5000,
 OPUS_FRAMESIZE_2_5_MS,
 OPUS_FRAMESIZE_5_MS,
 OPUS_FRAMESIZE_10_MS,
 OPUS_FRAMESIZE_20_MS,
 OPUS_FRAMESIZE_40_MS,
 OPUS_FRAMESIZE_60_MS,
 OPUS_FRAMESIZE_80_MS,
 OPUS_FRAMESIZE_100_MS,
 OPUS_FRAMESIZE_120_MS

} opus_more_stuff_t;


typedef int (*ope_write_func)(void *user_data, const unsigned char *ptr, int32_t len);
typedef int (*ope_close_func)(void *user_data);
typedef void (*ope_packet_func)(void *user_data, const unsigned char *packet_ptr, int32_t packet_len, uint32_t flags);


typedef struct _opus_enc_callbacks{
  ope_write_func write;
  ope_close_func close;
} OpusEncCallbacks;


")


(ffi.cdef "

 void *ope_comments_create();

 int ope_comments_add(void *comments, const char *tag, const char *val);

 void * ope_encoder_create_pull(void *comments, int32_t rate, int channels, int family, int *error);

 void * ope_encoder_create_callbacks(const OpusEncCallbacks *callbacks, 
                               void *user_data,
                              void *comments, 
                              int32_t rate, 
                             int channels, 
                             int family, 
                             int *error);


 int ope_encoder_get_page(void *enc, unsigned char **page, int32_t *len, int flush);

 int ope_encoder_ctl(void *enc, int request, ...);

 const char *ope_strerror(int error);

 int ope_encoder_write(void *enc, const int16_t *pcm, int samples_per_channel);

 int ope_encoder_drain(void *enc);

 void ope_comments_destroy(void *comments);

 void ope_encoder_destroy(void *enc);

")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{: libopus
 : libopusenc}

