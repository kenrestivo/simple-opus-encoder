(local ffi (require :ffi))
(local libasound (ffi.load :libasound))

;; from /usr/include/alsa/pcm.h
(ffi.cdef "

typedef enum _snd_pcm_format {
 SND_PCM_FORMAT_UNKNOWN = -1,
 SND_PCM_FORMAT_S8 = 0,
 SND_PCM_FORMAT_U8,
 SND_PCM_FORMAT_S16_LE,
 SND_PCM_FORMAT_S16_BE,
 SND_PCM_FORMAT_U16_LE,
 SND_PCM_FORMAT_U16_BE,
 SND_PCM_FORMAT_S24_LE,
 SND_PCM_FORMAT_S24_BE,
 SND_PCM_FORMAT_U24_LE,
 SND_PCM_FORMAT_U24_BE,
 SND_PCM_FORMAT_S32_LE,
 SND_PCM_FORMAT_S32_BE,
 SND_PCM_FORMAT_U32_LE,
 SND_PCM_FORMAT_U32_BE,
 SND_PCM_FORMAT_FLOAT_LE,
 SND_PCM_FORMAT_FLOAT_BE,
 SND_PCM_FORMAT_FLOAT64_LE,
 SND_PCM_FORMAT_FLOAT64_BE,
 SND_PCM_FORMAT_IEC958_SUBFRAME_LE,
 SND_PCM_FORMAT_IEC958_SUBFRAME_BE,
 SND_PCM_FORMAT_MU_LAW,
 SND_PCM_FORMAT_A_LAW,
 SND_PCM_FORMAT_IMA_ADPCM,
 SND_PCM_FORMAT_MPEG,
 SND_PCM_FORMAT_GSM,
 SND_PCM_FORMAT_S20_LE,
 SND_PCM_FORMAT_S20_BE,
 SND_PCM_FORMAT_U20_LE,
 SND_PCM_FORMAT_U20_BE,
 SND_PCM_FORMAT_SPECIAL = 31,
 SND_PCM_FORMAT_S24_3LE = 32,
 SND_PCM_FORMAT_S24_3BE,
 SND_PCM_FORMAT_U24_3LE,
 SND_PCM_FORMAT_U24_3BE,
 SND_PCM_FORMAT_S20_3LE,
 SND_PCM_FORMAT_S20_3BE,
 SND_PCM_FORMAT_U20_3LE,
 SND_PCM_FORMAT_U20_3BE,
 SND_PCM_FORMAT_S18_3LE,
 SND_PCM_FORMAT_S18_3BE,
 SND_PCM_FORMAT_U18_3LE,
 SND_PCM_FORMAT_U18_3BE,
 SND_PCM_FORMAT_G723_24,
 SND_PCM_FORMAT_G723_24_1B,
 SND_PCM_FORMAT_G723_40,
 SND_PCM_FORMAT_G723_40_1B,
 SND_PCM_FORMAT_DSD_U8,
 SND_PCM_FORMAT_DSD_U16_LE,
 SND_PCM_FORMAT_DSD_U32_LE,
 SND_PCM_FORMAT_DSD_U16_BE,
 SND_PCM_FORMAT_DSD_U32_BE,
 SND_PCM_FORMAT_LAST = SND_PCM_FORMAT_DSD_U32_BE,
 SND_PCM_FORMAT_S16 = SND_PCM_FORMAT_S16_LE,
 SND_PCM_FORMAT_U16 = SND_PCM_FORMAT_U16_LE,
 SND_PCM_FORMAT_S24 = SND_PCM_FORMAT_S24_LE,
 SND_PCM_FORMAT_U24 = SND_PCM_FORMAT_U24_LE,
 SND_PCM_FORMAT_S32 = SND_PCM_FORMAT_S32_LE,
 SND_PCM_FORMAT_U32 = SND_PCM_FORMAT_U32_LE,
 SND_PCM_FORMAT_FLOAT = SND_PCM_FORMAT_FLOAT_LE,
 SND_PCM_FORMAT_FLOAT64 = SND_PCM_FORMAT_FLOAT64_LE,
 SND_PCM_FORMAT_IEC958_SUBFRAME = SND_PCM_FORMAT_IEC958_SUBFRAME_LE,
 SND_PCM_FORMAT_S20 = SND_PCM_FORMAT_S20_LE,
 SND_PCM_FORMAT_U20 = SND_PCM_FORMAT_U20_LE,
} snd_pcm_format_t;

typedef enum _snd_pcm_stream {
 SND_PCM_STREAM_PLAYBACK = 0,
 SND_PCM_STREAM_CAPTURE,
 SND_PCM_STREAM_LAST = SND_PCM_STREAM_CAPTURE
} snd_pcm_stream_t;

typedef enum _snd_pcm_access {
 /** mmap access with simple interleaved channels */
 SND_PCM_ACCESS_MMAP_INTERLEAVED = 0,
 /** mmap access with simple non interleaved channels */
 SND_PCM_ACCESS_MMAP_NONINTERLEAVED,
 /** mmap access with complex placement */
 SND_PCM_ACCESS_MMAP_COMPLEX,
 /** snd_pcm_readi/snd_pcm_writei access */
 SND_PCM_ACCESS_RW_INTERLEAVED,
 /** snd_pcm_readn/snd_pcm_writen access */
 SND_PCM_ACCESS_RW_NONINTERLEAVED,
 SND_PCM_ACCESS_LAST = SND_PCM_ACCESS_RW_NONINTERLEAVED
} snd_pcm_access_t;

typedef unsigned long snd_pcm_uframes_t;

/// Functions

int snd_pcm_open(void **pcm, const char *name,
   snd_pcm_stream_t stream, int mode);


int snd_pcm_hw_params_malloc(void **ptr);

int snd_pcm_hw_params_any(void *pcm, void *params);

int snd_pcm_hw_params_set_access(void *pcm, void *params, snd_pcm_access_t _access);

int snd_pcm_hw_params_set_format(void *pcm, void *params, snd_pcm_format_t val);

int snd_pcm_hw_params_set_rate_near(void *pcm, void *params, unsigned int *val, int *dir);

int snd_pcm_hw_params_set_period_size  (
  void *   pcm,
  void *   params,
  snd_pcm_uframes_t   val,
  int   dir
 );

int snd_pcm_hw_params_get_rate  (  void *   params,
  unsigned int *   val,
  int *   dir
 );


int snd_pcm_hw_params_get_period_time  (  void *   params,
  unsigned int *   val,
  int *   dir
 );


int snd_pcm_hw_params_get_period_size  ( void *   params,
  snd_pcm_uframes_t *   val,
  int *   dir
 );

int snd_pcm_hw_params_set_channels(void *pcm, void *params, unsigned int val);

int snd_pcm_hw_params(void *pcm, void *params);

void snd_pcm_hw_params_free(void *obj);

int snd_pcm_prepare(void *pcm);

int snd_pcm_format_width(snd_pcm_format_t format);

long snd_pcm_readi(void *pcm, void *buffer, unsigned long size);

int snd_pcm_close(void *pcm);


")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; return the library
libasound
