(local io (require :io))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))
(local utils (require :utils))
(require-macros :utils-macros)

;; TODO: will need an endless read for testing

(fn read
  [params]
  (local fennel (require :fennel))
  (table.insert (or package.loaders package.searchers) fennel.searcher)
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        io (require :io)
        utils (require :utils)
        ctx (zthreads.get_parent_ctx)
        skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.pcm-addr}))
        ctl (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.file-in-control-addr}))
        file-in-socket (lua-dual-return (io.open params.file-in-path "r"))
        recv (fn
               [skt]
               (let [buf (file-in-socket:read params.bufsiz)]
                 (if buf
                     (do
                         (utils.log "file-in <- " (buf:len))
                       (skt:send buf)
                       ;; keep going until EOF
                       (recv skt))
                     ;; end this thing
                     (do
                         (utils.log "file-in got EOF, sending done message and ending")
                       (skt:send_more "")
                       (skt:send "DONE")))))]
    
    (utils.log "file-in connected => " params.pcm-addr)

    (ctl:send :ready)
    (utils.log "file-in setup,waiting for start")
    (utils.wait-for-start ctl) 
    (utils.log "file-in reading <= " params.file-in-path)
    
    (recv skt) ;; recurses until EOF
    (utils.log "file-in recv loop done")
    ;; TODO: linger?
    (skt:close)
    (ctl:close)
    (file-in-socket:close) ))



(fn write
  [params]
  (local fennel (require :fennel))
  (table.insert (or package.loaders package.searchers) fennel.searcher)
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        zpoller (require :lzmq.poller)
        io (require :io)
        utils (require :utils)
        ctx (zthreads.get_parent_ctx)
        skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :bind params.opus-addr}))
        ctl (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.file-out-control-addr}))
        file-out-socket (io.open params.file-out-path "w")
        poller (zpoller.new 1) ;; just 1 for now!
        recv (fn recv
               [in-skt]
               (let [(msg more) (in-skt:recv)]
                 (if (and msg (= msg "DONE"))
                     (do
                         (utils.log "file out got DONE, ending thread")
                       (poller:stop))
                     (do
                         (when msg
                           (let [l (msg:len)]
                             (utils.log "file-out <- " l)
                             ;; TODO: error czeching? seems to always return true!!
                             (file-out-socket:write msg)))))))]

    (poller:add skt zmq.POLLIN recv)
    
    (utils.log "file-out writing =>" params.file-out-path)
    
    (utils.log "file-out bound <= " params.opus-addr)

    (ctl:send :ready)
    (utils.log "file-out setup,waiting for start")
    (utils.wait-for-start ctl)
    
    (utils.log "file-out starting poller loop")
    (poller:start)
    
    (utils.log "file-out recv loop done")
    ;; TODO: linger?
    (skt:close)
    (ctl:close)
    (file-out-socket:flush)
    (file-out-socket:close)))


(fn setup-out
  [params]
  (tset params :file-out-ctl-skt (zmq.assert (params.context:socket {1 zmq.PAIR
                                                                     :bind params.file-out-control-addr})))
  (utils.log "creating file out thread")
  (tset params :file-out-thread (zthreads.run
                                 params.context
                                 write
                                 {:file-out-control-addr params.file-out-control-addr
                                  :file-out-path params.file-out-path
                                  :opus-addr params.opus-addr}))
  params)



(fn teardown-out
  [params]
  (params.file-out-thread:join)
  (tset params :file-out-thread nil)
  (params.file-out-ctl-skt:close)
  (tset params :file-out-ctl-skt nil)
  params)



(fn setup-in
  [params]

  (tset params :file-in-ctl-skt (zmq.assert (params.context:socket {1 zmq.PAIR
                                                                    :bind params.file-in-control-addr})))
  (utils.log "creating file read thread")
  (tset params :file-in-thread (zthreads.run
                                params.context
                                read
                                {:bufsiz params.bufsiz
                                 :file-in-control-addr params.file-in-control-addr
                                 :file-in-path params.file-in-path
                                 :pcm-addr params.pcm-addr}))
  params)


(fn teardown-in
  [params]
  (params.file-in-thread:join)
  (tset params :file-in-thread nil)
  (params.file-in-ctl-skt:close)
  (tset params :file-in-ctl-skt nil)
  params)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{: setup-in
 : setup-out
 : teardown-in
 : teardown-out}


