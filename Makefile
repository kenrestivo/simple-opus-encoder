# stolen shamelessly from alsa player but then  modified to be more pkcgonfigish

#CFLAGS=-O0 -g -pipe -Wall -ffast-math -fsigned-char -Wextra  # FOR GRINDING
CFLAGS=-O2 -pipe -Wall -ffast-math -fsigned-char -Wextra -fPIC

CFLAGS := $(CFLAGS) $(shell pkg-config --cflags alsa)
CFLAGS := $(CFLAGS) $(shell pkg-config --cflags libopusenc)
CFLAGS := $(CFLAGS) $(shell pkg-config --cflags opus)


PREFIX ?=	/usr/local
BINDIR ?=	$(PREFIX)/bin
MAN1DIR ?=	$(PREFIX)/man/man1

CC ?=		gcc

PCM_TMP=    /tmp/input.pcm
HIRES_TMP= /tmp/hi-res.pcm

INSTALL =	install
OUTPUT= /tmp/output.opus

all:: test-repeatable button.dtbo button-h3.dtbo events.lua



test-repeatable:: $(PCM_TMP)
	@echo "killing old opuses"
	-killall -w lua
	rm -f $(OUTPUT)
	./fennel tests/test-opus.fnl
	@ls -la 
	opusinfo $(OUTPUT)


test-hires:: $(HIRES_TMP)
	@echo "killing old opuses"
	-killall -w lua
	rm -f $(OUTPUT)
	./fennel tests/test-hires.fnl
	@ls -la $(OUTPUT)
	opusinfo $(OUTPUT)


test-file:: $(PCM_TMP)
	@echo "killing old opuses"
	-killall -w lua
	rm -f $(OUTPUT)
	./fennel tests/test-file.fnl
	@ls -la $(OUTPUT)
	opusinfo $(OUTPUT)


test-alsa::
	echo "killing old opuses"
	-killall -w lua
	@echo "===== Running test"
	./fennel tests/test-alsa.fnl
	@ls -la $(OUTPUT)
	opusinfo $(OUTPUT)



test-network::
	echo "killing old opuses"
	-killall -w lua
	@echo "===== Running test"
	./fennel tests/test-network.fnl


test-repeatable-network: $(HIRES_TMP)
	echo "killing old opuses"
	-killall -w lua
	@echo "===== Running test"
	./fennel tests/test-repeatable-network.fnl


headers:  headers/alsa-headers.h headers/opus-headers.h headers/input-headers.h 


## Specific targets

/tmp/hi-res.flac: 
	wget -c -O /tmp/hi-res.flac https://archive.org/download/KenRestivoInternetArchivePart1/on-strike-0.1.1.flac

events.lua: events-headers.fnl events.fnl utils.fnl utils-macros.fnl
	./fennel --compile --require-as-include  events.fnl > events.lua

$(PCM_TMP): test-data/test.wav.bz2
	bzcat test-data/test.wav.bz2 | sox -t wav - --bits 16 --rate 48000 --channels 2 -t raw  $(PCM_TMP)

## General rules

%.h: %.c
	gcc $(CFLAGS) -P -E $< > $@

%.wav: %.flac
	sox  $< $@ rate 48000

%.pcm: %.wav
	sox -t wav $< --bits 16 --rate 48000 --channels 2 -t raw $@


%.dtbo: %.dtsi
	dtc -@  -I dts -O dtb -o $@ $<
