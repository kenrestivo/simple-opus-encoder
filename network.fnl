(local socket (require :socket))
(local zmq (require :lzmq))
(local lume (require :lume))
(local zthreads (require :lzmq.threads))
(local utils (require :utils))
(local network-utils (require :network-utils))
(require-macros :utils-macros)




(fn write
  [params]
  (local fennel (require :fennel))
  (table.insert (or package.loaders package.searchers) fennel.searcher)
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        zpoller (require :lzmq.poller)
        io (require :io)
        utils (require :utils)
        network-utils (require :network-utils)
        ctx (zthreads.get_parent_ctx)
        skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :bind params.opus-addr}))
        ctl (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.network-control-addr}))
        ;; TODO ;error czeching, assert!
        conn (network-utils.login params)
        poller (zpoller.new 1) ;; just 1 for now!
        recv (fn recv
               [in-skt]
               (let [(msg more) (in-skt:recv)]
                 (if (and msg (= msg "DONE"))
                     (do
                         (utils.log "network got DONE, ending thread")
                       (poller:stop))
                     (do
                         (when msg
                           (let [l (msg:len)]
                             (utils.log "network <- " l)
                             ;; TODO: error czeching! especially for if the thing times out, i want to know!
                             (network-utils.send-ready? conn params.timeout-secs)
                             ;; TODO: more error checking here, check return vals
                             (conn:send msg)))))))]

    (poller:add skt zmq.POLLIN recv)
    
    (utils.log "network writing =>" params.server-addr ":" params.server-port)
    
    (utils.log "network bound <= " params.opus-addr)

    (ctl:send :ready)
    (utils.log "network setup,waiting for start")
    (utils.wait-for-start ctl)
    
    (utils.log "network starting poller loop")
    (poller:start)
    
    (utils.log "network recv loop done")
    ;; TODO: linger?
    (skt:close)
    (ctl:close)
    (conn:close)))


(fn setup
  [params]
  (tset params :network-ctl-skt (zmq.assert (params.context:socket {1 zmq.PAIR
                                                                    :bind params.network-control-addr})))
  (utils.log "creating network thread")
  (tset params :network-thread (zthreads.run
                                params.context
                                write
                                {:artist params.artist
                                 :basic-auth params.basic-auth ;; NO, only temporarily!
                                 :description params.description
                                 :genre params.genre
                                 :mountpoint params.mountpoint
                                 :network-control-addr params.network-control-addr
                                 :opus-addr params.opus-addr
                                 :server-addr params.server-addr
                                 :server-port params.server-port
                                 :timeout-secs params.timeout-secs
                                 :title params.title
                                 }))
  params)



(fn teardown
  [params]
  (params.network-thread:join)
  (tset params :network-thread nil)
  (params.network-ctl-skt:close)
  (tset params :network-ctl-skt nil)
  params)





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{: setup
 : teardown}


