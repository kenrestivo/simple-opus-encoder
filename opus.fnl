(local ffi (require :ffi))
(local lib (require :lib-opus))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))
(local utils (require :utils))
(require-macros :utils-macros)

(fn enc
  [params]
  (local fennel (require :fennel))
  (table.insert (or package.loaders package.searchers) fennel.searcher)
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        zpoller (require :lzmq.poller)
        utils (require :utils)
        ffi (require :ffi)
        lib (require :lib-opus)
        poller (zpoller.new 1) ;; just 1 for now!
        ctx (zthreads.get_parent_ctx)
        in-skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                        :bind params.pcm-addr}))
        out-skt (zmq.assert (ctx:socket {1 zmq.PAIR
                                         :connect params.opus-addr}))
        ctl (zmq.assert (ctx:socket {1 zmq.PAIR
                                     :connect params.opus-control-addr}))
        opus-err (ffi.new "int[1]")
        callbacks (ffi.new "OpusEncCallbacks[1]") 
        comments (syscall-non-nil (lib.libopusenc.ope_comments_create))]

    (utils.log "opus bound <= " params.pcm-addr)
    (utils.log "opus connected => " params.opus-addr)

    (fn opus-flags
      [enc flag val]
      "Takes encoder, a flag to set, and value.
       Casting appears to be critical."
      (syscall-pos (lib.libopusenc.ope_encoder_ctl enc flag (ffi.new "int32_t" val))))
    
    (-> callbacks
        (.  0)
        (tset :write (ffi.cast "ope_write_func" (fn [info ptr len]
                                                  (utils.log "opus -> " len)
                                                  ;; TODO; error czeching? is there an assert?
                                                  (out-skt:send (ffi.string ptr len))
                                                  0 ))))
    (-> callbacks
        (. 0)
        (tset :close (ffi.cast "ope_close_func" (fn [info]
                                                  (utils.log "opus sending DONE message, stopping")
                                                  (out-skt:send_more "")
                                                  (out-skt:send "DONE")
                                                  0))))
    (utils.log "opus adding comments")
    (syscall-pos (lib.libopusenc.ope_comments_add comments "ARTIST" params.artist))
    (syscall-pos (lib.libopusenc.ope_comments_add comments "TITLE" params.title))

    (local enc (syscall-non-nil
                (lib.libopusenc.ope_encoder_create_callbacks
                 callbacks
                 (ffi.cast "void *" :foo) ;; nothing really
                 comments
                 params.sample-rate
                 params.channels
                 params.mapping
                 opus-err)))


    
    
    (utils.log "opus setting flags")
    (opus-flags enc lib.libopus.OPUS_SET_COMPLEXITY_REQUEST  params.complexity)
    (opus-flags enc lib.libopus.OPUS_SET_APPLICATION_REQUEST params.application)
    (opus-flags enc lib.libopus.OPUS_SET_LSB_DEPTH_REQUEST params.bit-depth)
    (opus-flags enc lib.libopus.OPUS_SET_BITRATE_REQUEST params.bit-rate)

    
    
    (fn recv
      [in-skt]
      "Recv callback. Must be defined after enc, as it closes over it (or curry it in)"
      (let [(msg more) (in-skt:recv)]
        (if (and msg (= msg "DONE"))
            (do
                (utils.log "opus got DONE, ending thread")
              (poller:stop))
            (do
                (if msg
                    (let [l (msg:len)]
                      (utils.log "opus <- " l)
                      (syscall-pos
                       (lib.libopusenc.ope_encoder_write enc
                                                         (ffi.cast "uint16_t *" msg)
                                                         (/ l
                                                            params.bytes-per-channel
                                                            params.channels))))
                    (utils.log "opus got a NULL msg"))))))


    (utils.log "opus sending ready")
    (poller:add in-skt zmq.POLLIN recv)
    
    (ctl:send :ready)
    (utils.log "opus setup,waiting for start")
    (utils.wait-for-start ctl)
    (utils.log "opus starting receive  loop")

    (poller:start)
    
    (utils.log "opus recv loop done")
    
    (syscall-pos (lib.libopusenc.ope_encoder_drain enc))

    ;; Just to be absolutely sure! don't leak!
    (-> callbacks
        (. 0 :write)
        (: :free))
    (-> callbacks
        (. 0 :close)
        (: :free))
    
    ;; TODO: linger?
    (utils.log "opus thread ending, sockets closing")
    (in-skt:close)
    (out-skt:close)
    (ctl:close)))



(fn setup
  [params]
  (tset params :opus-ctl-skt (params.context:socket {1 zmq.PAIR
                                                     :bind params.opus-control-addr}))
  (utils.log "creating opus thread")
  (tset params :opus-thread (zthreads.run
                             params.context
                             enc
                             {:application params.application
                              :artist params.artist
                              :bit-depth params.bit-depth
                              :bit-rate params.bit-rate
                              :bytes-per-channel params.bytes-per-channel
                              :channels params.channels
                              :complexity params.complexity
                              :mapping params.mapping
                              :opus-addr params.opus-addr
                              :opus-control-addr params.opus-control-addr
                              :pcm-addr params.pcm-addr
                              :sample-rate params.sample-rate
                              :title params.title}))
  params)


(fn teardown
  [params]
  (params.opus-thread:join)
  (tset params :opus-thread nil)
  (params.opus-ctl-skt:close)
  (tset params :opus-ctl-skt nil)
  params)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{:setup setup
 :libs lib
 :teardown teardown}
