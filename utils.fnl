(local io (require :io))
(local ffi (require :ffi))
(require-macros :utils-macros)

;; Needed for the macros :(
(ffi.cdef "
	char *strerror(int errnum);
")

(fn log
  [...]
  (each [i v (ipairs [...])]
    (: io.stderr :write v))
  (: io.stderr :write "\n"))



(fn blocking-wait-start
  [skt]
  (: skt :recv)
  (: skt :send :start))


(fn wait-for-start
  [skt]
  (when (not (= :start (: skt :recv)))
    (log "waiting for start, got something other than start")
    (wait-for-start skt)))


(fn slurp
  [filename]
  (let [f (lua-dual-return (io.open filename "rb"))
        s (f:read "*all")]
    (f:close)
    s))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

{: log
 : blocking-wait-start
 : slurp
 : wait-for-start}

