(local fennel (require :fennel))
(local lume (require :lume))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local alsa (require :alsa))
(local file (require :file))
(local opus (require :opus))
(local utils (require :utils))
(local zmq (require :lzmq))
(local zthreads (require :lzmq.threads))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local state {:params {
                       :application opus.libs.libopus.OPUS_APPLICATION_AUDIO
                       :artist "Testing Opus Encoding"
                       :bit-depth 16 ;; we know these things,so hardcoding them, but different files might not
                       :bit-rate 96000
                       :bufsiz 4096
                       :bytes-per-channel 2 ;; we know these things,so hardcoding them,but different files might not
                       :channels 2
                       :complexity 10
                       :device-name "hw:1,0"
                       :file-in-path "/tmp/input.pcm"
                       :file-out-path "/tmp/output.opus"
                       :format alsa.lib.SND_PCM_FORMAT_S16_LE
                       :frames-to-buffer 1024
                       :mapping 0 ;;; used for opus
                       :opus-addr "inproc://opus"
                       :pcm-addr "inproc://pcm"
                       :opus-control-addr "inproc://opus-control"
                       :file-in-control-addr "inproc://file-in-control"
                       :file-out-control-addr "inproc://file-out-control"
                       :sample-rate 48000 ;; we know these things,so hardcoding them, but different files might not
                       :title "Test Sample"
                       }})



(fn start
  [state]
  (tset state :params (file.setup-in state.params))
  (tset state :params (file.setup-out state.params))
  (tset state :params (opus.setup state.params))


  ;; horrilble, but works
  (: state.params.file-out-thread :start)
  (: state.params.file-out-ctl-skt :recv)


  (: state.params.opus-thread :start)
  (: state.params.opus-ctl-skt :recv)

  (: state.params.file-in-thread :start)
  (: state.params.file-in-ctl-skt :recv)


  (: state.params.file-out-ctl-skt :send :start)
  (: state.params.opus-ctl-skt :send :start)
  (: state.params.file-in-ctl-skt :send :start)
  state)

(fn stop
  [state]  
  (tset state :params (file.teardown-out state.params))
  (tset state :params (opus.teardown state.params))
  (tset state :params (file.teardown-in state.params))
  state)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state.params :context  (zmq.context))

(start state)
(stop state)

(: state.params.context :term)



