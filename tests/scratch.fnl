(global fennel (require :fennel))
(global lume (require :lume))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(global alsa (require :alsa))
(global file (require :file))
(global opus (require :opus))
(global zmq (require :lzmq))
(global zthreads (require :lzmq.threads))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global state {:params {
                        :application opus.libs.libopus.OPUS_APPLICATION_AUDIO
                        :artist "Testing Opus Encoding"
                        :bit-depth 16
                        :bit-rate 96000
                        :bufsiz 4096
                        :channels 2
                        :device-name "hw:0,0"
                        :file-in-path "/tmp/input.pcm"
                        :file-out-path "/tmp/output.opus"
                        :format alsa.lib.SND_PCM_FORMAT_S16_LE
                        :frames-to-buffer 1024
                        :mapping 0 ;;; used for opus
                        :opus-addr "inproc://opus"
                        :pcm-addr "inproc://pcm"
                        :sample-rate 48000
                        :title "Test Sample"
                        }
               })

(tset state.params :context  (zmq.context))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; all setups

(tset state :params (alsa.setup state.params))
(tset state :params (file.setup-out state.params))
(tset state :params (file.setup-in state.params))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; all tearddowns
(tset state :params (opus.teardown state.params))
(tset state :params (alsa.teardown state.params))
(tset state :params (file.teardown-out state.params))
(tset state :params (file.teardown-in state.params))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state :params (alsa.setup state.params))
(tset state :params (alsa.teardown state.params))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state :params (file.setup-out state.params))
(tset state :params (file.teardown-out state.params))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state :params (file.setup-in state.params))
(tset state :params (file.teardown-in state.params))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(tset state :params (opus.setup state.params))
(tset state :params (opus.teardown state.params))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global ffi (require :ffi))

(lume.hotswap :file)
(lume.hotswap :lib-opus)

;; TODO: can use the lua io with this to convert the buffer (may be slow? unknown)
ffi.string


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(-> zmq lume.keys lume.sort)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(-> zthreads lume.keys lume.sort)



;; XXX not wquite right
(print (fennel.compileString xxx))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THIS WORKS

(local ctx (zmq.context))
(local skt (: ctx :socket zmq.PAIR))
(global addr "inproc://test.inproc")
(zmq.assert (: skt :bind addr))

(fn foo
  [addr]
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        ctx (zthreads.get_parent_ctx)
        skt (: ctx :socket zmq.PAIR)]
    (zmq.assert (: skt :connect addr))
    (: skt :send "hello")
    (print "thread: " (: skt :recv))
    (: skt :close)
    (: ctx :term)))

(local thread (zthreads.run ctx foo addr))

;; now it's detached and joinable
(: thread :start) 

(print "main: " (: skt :recv))

(: skt :send "wtf")

(: skt :type)
(: thread :detached)
(: thread :joinable)


(: thread :join)
(: skt :close)
(: ctx :term)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; multi pass

(global zmq (require :lzmq))
(global zthreads (require :lzmq.threads))

(local ctx (zmq.context))
(local skt (: ctx :socket zmq.PAIR))
(global args {:addr "inproc://test.inproc"})
(zmq.assert (: skt :bind args.addr))


(fn foo
  [args]
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        ctx (zthreads.get_parent_ctx)
        skt (: ctx :socket zmq.PAIR)
        recv (fn 
               [skt]
               (let [(msg more) (: skt :recv)]
                 (print "thread: " msg)
                 (if more
                     (: skt :recv) ;; throw away, and go away
                     (recv skt))))]
    (zmq.assert (: skt :connect args.addr))
    (print "thread connected")
    (recv skt)
    (: skt :close)
    (: ctx :term)))

(local thread (zthreads.run ctx foo args))

;; now it's detached and joinable
(: thread :start) 


(: skt :send "wtf")

(: skt :type)
(: thread :detached)
(: thread :joinable)

;; stop the loop
(: skt :send_more "hey hey bye bye")
(: skt :send "")


(: thread :join)
(: skt :close)
(: ctx :term)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; why  this
;; .. [ "\1" false ]
;; >> .. [ "\1" false ] >>
;; .. { 2 #<[ZMQ][EAGAIN] Resource temporarily unavailable (11)> }
;; >> .. [ "wtf" false ] 
;; >> .. [ "wtf" false ]
;; >> .. [ "wtf" false ]
;; >> .. [ "wtf" false ]
;; >> .. [ "wtf" false ]


(global zmq (require :lzmq))
(global zthreads (require :lzmq.threads))
(global addr "inproc://test.inproc")

(local ctx (zmq.context))
(local skt (: ctx :socket {1 zmq.SUB
                           :subscribe ""
                           :bind addr}))

(fn foo
  [addr]
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        ctx (zthreads.get_parent_ctx)
        skt (: ctx :socket {1 zmq.PUB
                            :connect addr})]
    (print "thread connected")
    (while true
      (: skt :send "wtf")) 
    (: skt :close)
    (: ctx :term)))

(local thread (zthreads.run ctx foo addr))

;; now it's detached and joinable
(: thread :start) 


(let [(x y) (: skt :recv zmq.NOBLOCK)]
  [x y])

(: skt :type)
(: thread :detached)
(: thread :joinable)

;; stop the loop
(: skt :send_more "hey hey bye bye")
(: skt :send "")


(: thread :join)
(: skt :close)
(: ctx :term)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; let's try another one


(global zmq (require :lzmq))
(global zthreads (require :lzmq.threads))
(global args {:pub-addr "inproc://test.pub"
              :ctl-addr "inproc://test.inproc"})

(local ctx (zmq.context))
(local ctl-skt (: ctx :socket zmq.PAIR))
(zmq.assert (: ctl-skt :bind args.ctl-addr))


(local pubskt (: ctx :socket {1 zmq.SUB
                              :subscribe ""
                              :bind args.pub-addr}))

(fn foo
  [args]
  (let [zmq (require :lzmq)
        zthreads (require :lzmq.threads)
        ctx (zthreads.get_parent_ctx)
        ctl-skt (: ctx :socket zmq.PAIR)
        pub-skt (: ctx :socket {1 zmq.PUB
                                :connect args.pub-addr})
        recv (fn 
               [ctl-skt]
               (let [(msg more) (: ctl-skt :recv)]
                 (print "thread: " msg)
                 (: pub-skt :send msg)
                 (if more
                     (: ctl-skt :recv) ;; throw away, and go away
                     (recv ctl-skt))))]
    (zmq.assert (: ctl-skt :connect args.ctl-addr))
    (print "thread connected")
    (recv ctl-skt)
    (: ctl-skt :close)
    (: pub-skt :close)
    (: ctx :term)))

(local thread (zthreads.run ctx foo args))

;; now it's detached and joinable
(: thread :start) 


(: ctl-skt :send "wtf")

(let [(x y) (: pubskt :recv zmq.NOBLOCK)]
  [x y])

(: pubskt :type)
(: thread :detached)
(: thread :joinable)

;; stop the loop
(: skt :send_more "hey hey bye bye")
(: skt :send "")


(: thread :join)
(: skt :close)
(: ctx :term)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ffi.new "uint32_t [1]" 0)

(ffi.sizeof (ffi.new "long [1]" 0)) 
(ffi.sizeof (ffi.new "int [1]" 0))
(ffi.sizeof (ffi.new "unsigned int [1]" 0))
(ffi.sizeof (ffi.new "uint32_t [1]" 0))
(ffi.sizeof (ffi.new "uint64_t [1]" 0))
(ffi.sizeof (ffi.new "int64_t [1]" 0))


(ffi.new "int64_t" 128)

(tonumber (ffi.new "int64_t" 128))

(tonumber (* 2 (ffi.new "int64_t" 128)))

(tonumber (* 2 2 (ffi.new "int64_t" 1024)))

(ffi.new "int64_t" 4096)

(* (ffi.new "int64_t" 4096) 2)


(ffi.new "uint8_t[?]" 4096)

(ffi.sizeof (ffi.new "uint8_t[?]" 4096))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local fennelview (require :fennelview)) ;; doesn't work with local for some reason on some machines. why??
(local fennel (require :fennel))

(fennel.eval (fennelview {:foo
                          [1 32 9 ]
                          :baz [:ok 33]} {:detect-cycles? false
                                          :one-line true})) 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local fsm (require :fsm))

(local streamer (fsm.create {:initial :stopped
                             :events [{:name :connect
                                       :from :stopped
                                       :to :connecting}

                                      {:name :abandon ;; can't use name cancel, it's protected or something
                                       :from :connecting
                                       :to :stopped}

                                      {:name :connected
                                       :from :connecting
                                       :to :running}

                                      {:name :disconnect
                                       :from :running
                                       :to :stopped}
                                      
                                      {:name :stop
                                       :from :running
                                       :to :stopped}]}))


;; button press would take into account the current state, emit different events depending on it.
;; callbacks on connect would blink the light, callbacks on disconnect would go back to connecting, off would turn the light off
;; errors would also cause the light to blink maybe

;; more states! all the setup!


;; TODO: better to use pending and cancel built in to the library??

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(streamer:connect)
(streamer:connected)
(streamer:abandon)

streamer.current



(local wtf "[foo] bar baz")

(local wtf2 "foo [bar] baz")
